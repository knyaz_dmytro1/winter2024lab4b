import java.util.Scanner;
public class VirtualPetApp{
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        Monkey[] tribe = new Monkey[1];
		System.out.println("A new monkey joined the tribe, what will you name him?");
		String name = reader.nextLine();
		System.out.println(name + " that's a great choice! Now how does this monkey spends it's time?");
		String occupation = reader.nextLine();
		System.out.println(occupation + " wow that's an interesting way to pass time! Now finally for the last part... Is " + name + " a silly monkey? \nEnter true or false:");
		boolean isSilly = Boolean.parseBoolean(reader.nextLine());
        for(int i = 0; i<tribe.length; i++){
            tribe[i] = new Monkey(name, occupation, isSilly);
        }
		System.out.println(tribe[tribe.length - 1].getIsSilly());
        System.out.println(tribe[tribe.length - 1].getName());
        System.out.println(tribe[tribe.length - 1].getOccupation());
		System.out.println("Please rename your monkey:");
		tribe[0].setName(reader.nextLine());
        System.out.println(tribe[tribe.length - 1].getIsSilly());
        System.out.println(tribe[tribe.length - 1].getName());
        System.out.println(tribe[tribe.length - 1].getOccupation());


     

    }
}