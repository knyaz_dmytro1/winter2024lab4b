
public class Monkey{
    private String name;
    private String occupation;
    private boolean isSilly;
	public Monkey(String name, String occupation, boolean isSilly){
		this.name = name;
		this.occupation = occupation;
		this.isSilly = isSilly;
	}
    public void treeClimbing(){
        if (isSilly){
            System.out.println(name + " is attempting to climb a tree! Will he succeed?");
            System.out.println("YES ! " + name + " skillfully climbed his tree!");
        } else {
            System.out.println(name + " isn't silly enough to climb the tree.");
        }
    }
    public void bananaStealing(){
        System.out.println(name + " decided to drop it's occupation of " + occupation + " to steal bananas instead");
        System.out.println("He successfully stole 5 bananas");
    }
	public void setName(String name){
		this.name = name;
	}
	/*
	public void setOccupation(String occupation){
		this.occupation = occupation;
	}
	public void setIsSilly(boolean isSilly){
		this.isSilly = isSilly;
	}
	*/
	public String getName(){
		return this.name;
	}
	public boolean getIsSilly(){
		return this.isSilly;
	}
	public String getOccupation(){
		return this.occupation;
	}
}